package de.bosc.fufeu.company.business;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Vector;

import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.NotesException;
import lotus.domino.View;

import de.bosc.fufeu.company.model.Person;
import de.bosc.fufeu.company.model.Standby;
import de.bosc.fufeu.company.model.StandbyCalendar;
import de.bosc.fufeu.company.model.StandbyCalendarDay;
import de.bosc.fufeu.company.model.TimeEntry;

public class StandbyCalendarService implements Serializable {

	private StandbyCalendar standbyCalendar = new StandbyCalendar();
	private DatabaseService databaseService;
	private int componentHeight = 150;

	
	public StandbyCalendarService( DatabaseService databaseService) {
		StringBuilder sb = new StringBuilder();
		try {
			this.databaseService = databaseService;

			for(Standby standby:this.databaseService.getStandbyService().getListStandby()){
				standbyCalendar.setCurrentStandby(standby);
				standbyCalendar.getListStandby().put(standby.getDocid(), standby);
				sb.append("\n" + standby);
			}
			System.out.println("" + sb);
			this.databaseService.debug("standbyCalendar.getCurrentStandby(): " + standbyCalendar.getCurrentStandby() + sb);
			standbyCalendar.setSelectedPerson(databaseService.getPersonService().getCurrentPerson());
			this.initStandbyCalendar(standbyCalendar.getSelectedDate(), standbyCalendar.getCurrentStandby());
		} catch (Exception e) {
			databaseService.error(e);
		}
		
	}
	
	public void initStandbyCalendar(Date date, Standby standby){
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		int currentMonth = cal.get(Calendar.MONTH);
		
		int goBackDays = cal.get(Calendar.DAY_OF_WEEK);
		databaseService.debug("date: " + cal.getTime());
		databaseService.debug("goBackDays: " + goBackDays);
		if(goBackDays == 1){
			goBackDays = 6;
		}else{
			goBackDays = goBackDays-2;
		}
		
		databaseService.debug("Month: " + currentMonth);
		databaseService.debug("goBackDays: " + goBackDays);
		
		cal.add(Calendar.DAY_OF_MONTH, -1*goBackDays);
		
		TreeMap<Integer,StandbyCalendarDay> days = new TreeMap<Integer,StandbyCalendarDay>();
		HashMap<String,TimeEntry> timeEntries = getStandbyTimeEntries(cal.getTime(),standbyCalendar.getCurrentStandby());
		
		for(int i = 0;i<42;i++){
			StandbyCalendarDay standbyCalendarDay = new StandbyCalendarDay(new Integer(i),cal.getTime(),cal.get(Calendar.MONTH) == currentMonth,standbyCalendar.getCurrentStandby());
//			service.log("--- initStandbyCalendar key:" + TimeEntryService.dateFormat.format(cal.getTime())+ "true   ----- " + (timeEntries.get(TimeEntryService.dateFormat.format(cal.getTime())+ "true")!=null));
//			service.log("--- initStandbyCalendar key:" + TimeEntryService.dateFormat.format(cal.getTime())+ "false   ----- " + (timeEntries.get(TimeEntryService.dateFormat.format(cal.getTime())+ "false")!=null));
			standbyCalendarDay.setFirstTimeEntry(timeEntries.get(TimeEntryService.dateFormat.format(cal.getTime())+ "true"));
			standbyCalendarDay.setSecondTimeEntry(timeEntries.get(TimeEntryService.dateFormat.format(cal.getTime())+ "false"));
			days.put(new Integer(i), standbyCalendarDay);			
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		databaseService.debug("standby: " + (standbyCalendar.getCurrentStandby().getStart()*60.0) + "height: " + ((standbyCalendar.getCurrentStandby().getStart()*60.0) / (24*60*60.0))*100);
		standbyCalendar.setDays(days);
	}
	
	
	public HashMap<String,TimeEntry> getStandbyTimeEntries(Date date, Standby standby){
		HashMap<String,TimeEntry> timeEntries = new HashMap<String, TimeEntry>();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		timeEntries = getStandbyTimeEntriesMonth(cal.getTime(), standby);
		cal.add(Calendar.MONTH, 1);
		timeEntries.putAll(getStandbyTimeEntriesMonth(cal.getTime(), standby));
		cal.add(Calendar.MONTH, 1);
		timeEntries.putAll(getStandbyTimeEntriesMonth(cal.getTime(), standby));
		return timeEntries;
	}

	public HashMap<String,TimeEntry> getStandbyTimeEntriesMonth(Date date, Standby standby){
		HashMap<String,TimeEntry> timeEntries = new HashMap<String, TimeEntry>();
		try {
			View  view = databaseService.getCurrentDatabase().getView("($timeentrieByMonthByTypes)");
			Vector<String> v = new Vector<String>();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
			v.add(dateFormat.format(date));
			v.add(standby.getDocid());
			DocumentCollection coll = view.getAllDocumentsByKey(v);
			databaseService.debug("getStandbyTimeEntries keys:" + v + " found: " + coll.getCount());
			Document doc = coll.getFirstDocument();
			while(doc != null){
				Document docNext = coll.getNextDocument(doc);
				TimeEntry timeEntry = this.databaseService.getTimeEntryService().getTimeEntryFromDoc(doc);
				if(!timeEntry.getPersonalnumber().equals("")){					
					Person person = databaseService.getPersonService().getPersonsByPersonalNumber().get(timeEntry.getPersonalnumber());
					timeEntry.setPerson(person);
				}
				timeEntries.put(timeEntry.getDate() + (timeEntry.getStarttime() == 0), timeEntry);
				databaseService.debug("--- getStandbyTimeEntries key:" + timeEntry.getDate() + (timeEntry.getStarttime() == 0) + " Person:" + (timeEntry.getPerson() != null));
				doc.recycle();
				doc = docNext;	
			}
		} catch (NotesException e) {
			databaseService.error(e);
		}
		return timeEntries;
	}
	
	public void setTimeEntryCalendarDay(StandbyCalendarDay standbyCalendarDay,boolean firstTimeEntry){
		try{
			if(standbyCalendarDay.getIndex() == 0 && firstTimeEntry){
				return;
			}
			setTimeEntry(standbyCalendarDay, firstTimeEntry);
			if(firstTimeEntry){
				StandbyCalendarDay standbyCalendarDayPrevious = standbyCalendar.getDays().get(standbyCalendarDay.getIndex() -1);
				setTimeEntry(standbyCalendarDayPrevious, false);
			}else{
				StandbyCalendarDay standbyCalendarDayNext = standbyCalendar.getDays().get(standbyCalendarDay.getIndex() + 1);
				setTimeEntry(standbyCalendarDayNext, true);
			}			
		}catch(Exception e){
			databaseService.error(e);
		}
	}

	public void setTimeEntry(StandbyCalendarDay standbyCalendarDay,boolean firstTimeEntry){
		TimeEntry timeEntry;
		int startTime = 0;
		int endTime = 0;
		if(firstTimeEntry){
			timeEntry = standbyCalendarDay.getFirstTimeEntry();
			startTime = 0;
			endTime = standbyCalendar.getCurrentStandby().getStart()*60;
		}else{
			timeEntry = standbyCalendarDay.getSecondTimeEntry();
			startTime = standbyCalendar.getCurrentStandby().getStart()*60;
			endTime = 24*60*60;
		}
		
		if(timeEntry != null && timeEntry.getPerson() != null){
			if(timeEntry.getPerson().getPersonalNumber().equals(standbyCalendar.getSelectedPerson().getPersonalNumber())){
				timeEntry.setPersonalnumber("");
				timeEntry.setCompany("");
				timeEntry.setDepartment("");
				timeEntry.setOrgFunctions(new ArrayList<String>());
				timeEntry.setPrio(0);
				timeEntry.setQualifications(new ArrayList<String>());
				timeEntry.setPerson(null);
			}else{
				timeEntry.setPersonalnumber(standbyCalendar.getSelectedPerson().getPersonalNumber());
				timeEntry.setPerson(standbyCalendar.getSelectedPerson());				
			}
		}else if(timeEntry != null){
			timeEntry.setPersonalnumber(standbyCalendar.getSelectedPerson().getPersonalNumber());
			timeEntry.setPerson(standbyCalendar.getSelectedPerson());			
		}else{
			databaseService.debug("StandbyCalendarService.setTimeEntry: " + standbyCalendarDay.getDate());
			timeEntry = this.databaseService.getTimeEntryService().createTimeEntry(standbyCalendarDay.getDate(), standbyCalendar.getSelectedPerson(), standbyCalendar.getCurrentStandby().getDocid());
			timeEntry.setPerson(standbyCalendar.getSelectedPerson());
		}
		
		timeEntry.setStarttime(startTime);
		timeEntry.setEndtime(endTime);
		
		try {
			this.databaseService.getTimeEntryService().saveTimeEntry(timeEntry);
		} catch (NotesException e) {
			databaseService.error(e);
		}

		if(firstTimeEntry){
			standbyCalendarDay.setFirstTimeEntry(timeEntry);
		}else{
			standbyCalendarDay.setSecondTimeEntry(timeEntry);
		}

	}
	
	
	public StandbyCalendar getStandbyCalendar() {
		return standbyCalendar;
	}


	public int getComponentHeight() {
		return componentHeight;
	}
	
	public void setSelectedPerson(String selectedPersonShortname) {
		standbyCalendar.setSelectedPerson( databaseService.getPersonService().getPersons().get(selectedPersonShortname));
	}
	public String getSelectedPerson(){
		return standbyCalendar.getSelectedPerson().getShortname();
	}

	public void setSelectedDate(Date selectedDate) {
		standbyCalendar.setSelectedDate(selectedDate);
		initStandbyCalendar(selectedDate,standbyCalendar.getCurrentStandby());
	}
	public void addSelectedDateMonth(int value){
		Calendar cal = Calendar.getInstance();
		cal.setTime(standbyCalendar.getSelectedDate());
		cal.add(Calendar.MONTH, value);
		standbyCalendar.setSelectedDate(cal.getTime());
		initStandbyCalendar(standbyCalendar.getSelectedDate(), standbyCalendar.getCurrentStandby());
	}
}
