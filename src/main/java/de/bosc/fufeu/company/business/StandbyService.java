package de.bosc.fufeu.company.business;

import java.io.Serializable;
import java.util.ArrayList;

import lotus.domino.Document;
import lotus.domino.View;

import de.bosc.fufeu.company.model.Standby;

public class StandbyService implements Serializable{

	
	private DatabaseService databaseService;
	

	public StandbyService(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}


	public ArrayList<Standby> getListStandby(){
		ArrayList<Standby> listStandby = new ArrayList<Standby>();
		try {
			View view = this.databaseService.getCurrentDatabase().getView("($standby)");
			Document doc = view.getFirstDocument();
			while(doc != null){
				Document docNext = view.getNextDocument(doc);
				Standby standby = new Standby();
				standby.setName(doc.getItemValueString("standbyname"));
				standby.setDisplayname(doc.getItemValueString("displayname"));
				standby.setAlias(doc.getItemValueString("alias"));
				standby.setDocid(doc.getItemValueString("docid"));
				standby.setActive(doc.getItemValueString("active").equals("1"));
				standby.setStart(doc.getItemValueInteger("start"));
				standby.setOrgFunction(doc.getItemValueString("orgFunction"));
				listStandby.add(standby);
				doc.recycle();
				doc = docNext;
			}
			view.recycle();
		} catch (Exception e) {
			this.databaseService.error(e);
		}

		return listStandby;
	}
	
}
