package de.bosc.fufeu.company.business;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import de.bosc.fufeu.company.model.Company;
import de.bosc.fufeu.company.model.Graph24h;
import de.bosc.fufeu.company.model.QualificationGroup;
import de.bosc.fufeu.company.model.TimeEntry;

public class Graph24hService implements Serializable {

	private static final long serialVersionUID = 1L;
	private Date selectedDate = new Date();
	private DatabaseService databaseService;
	private LinkedHashMap<Company,Graph24h.GraphData> mapCompanyData = null;;
	
	public Graph24hService(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}


	/**
	 * @param value
	 * Setzt das Datum um x Tag vor bzw bei negativem value zur�ck
	 */
	public void addDay(int value){
		Calendar c = Calendar.getInstance();
		c.setTime(selectedDate);
		c.add(java.util.Calendar.DATE, value);
		selectedDate = c.getTime();
	}
	
	
	public String getSvgGraphic24h(){
		
		// Map f�r Datenreihen nach Firmen getrennt
		mapCompanyData = new LinkedHashMap<Company, Graph24h.GraphData>();
		String svgGraphic = "Sorry... no data found";
		databaseService.debug("################### companyList:" + databaseService.getCompanyList().keySet());
		try{
			// Liste der Qualifikationen erstellen
			List<String> listQualifications = new ArrayList<String>();
			for(QualificationGroup qualificationGroup:databaseService.getMapQualificationGroupsGraph24().values()) {
				listQualifications.addAll(qualificationGroup.getList());
			}
			databaseService.debug("listQualifications: " + listQualifications + " : map = "+ databaseService.getMapQualificationGroupsGraph24().entrySet());
			Graph24h graph24h = new Graph24h(databaseService);
			
			// Liste aller ADHOC Zeiteintr�ge an einem Tag
			List<TimeEntry> timeEntries = this.databaseService.getTimeEntryService().getTimeEntriesAdhocforDay(selectedDate); 
			databaseService.debug("Graph24hService.getGraph24h: timeEntries:" + timeEntries.size());
			
			for(TimeEntry timeEntry:timeEntries){
				List<String> list = new ArrayList<String>();
				list.addAll(timeEntry.getQualifications());
				list.retainAll(new ArrayList(listQualifications));
				StringBuilder sb = new StringBuilder("entry: " + timeEntry.getPersonalnumber() + " -> " + list.size() + " -- " + listQualifications.size() + " : " + timeEntry.getQualifications());
				if(list.size()>0) {
					// Company ermitteln
					
					Company company = getDatabaseService().getCompany(timeEntry.getCompany());
					if(company == null && timeEntry.getPerson() != null){
						company = getDatabaseService().getCompany(timeEntry.getPerson().getCompany());
						sb.append("\n Graph24hService.getGraph24h: ----- company person:" + timeEntry.getPerson().getCompany() + " == " + (company != null) );
					}
					sb.append("\nGraph24hService.getGraph24h: ----- companyTimeEntry:" + timeEntry.getCompany() + " == " + (company != null) + " person = " + (timeEntry.getPerson() != null) );
					if( company != null){
						// Wenn die Company noch nicht in der Map vorhanden ist dann neue Datenreihe erzeugen
						Graph24h.GraphData graphData = mapCompanyData.get(company);
						sb.append("\n Graph24hService.getGraph24h: -------  graphData:" + company.getName() + " -- " + (graphData==null?"":graphData.getColor()) + " ------- " + mapCompanyData.keySet() );
						if(graphData == null){
							sb.append("\n Graph24hService.getGraph24h: ###### new company:" + company.getName() );
							graphData = graph24h.new GraphData();
							graphData.setColor(company.getColor());
							graphData.setTextcolor(company.getTextColor());
							mapCompanyData.put(company, graphData);
							sb.append("\n Graph24hService.getGraph24h: companies:" + mapCompanyData.size());
						}
						graphData.addTimeEntry(timeEntry,sb);
					}
				}
				databaseService.debug(sb.toString());

			}
			
			if(mapCompanyData.size() == 0){
				databaseService.warning("Graph24hService.getGraph24h: no data found add empty Array");
				Graph24h.GraphData graphData = graph24h.new GraphData();
				graph24h.addGraphData(graphData);
			}else{			
				for(Graph24h.GraphData graphData:mapCompanyData.values()){
					graph24h.addGraphData(graphData);
				}
			}
			svgGraphic = graph24h.getSvgGraphic();
		}catch(Exception e){
			databaseService.error(e);
		}
		this.mapCompanyData = mapCompanyData;
		
		return svgGraphic;
	}
	
	public DatabaseService getDatabaseService() {
		return databaseService;
	}
	public void setDatabaseService(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}

	public Date getSelectedDate() {
		return selectedDate;
	}
	public void setSelectedDate(Date selectedDate) {
		this.selectedDate = selectedDate;
	}
	public LinkedHashMap<Company, Graph24h.GraphData> getMapCompanyData() {
		return mapCompanyData;
	}
	public void setMapCompanyData(
			LinkedHashMap<Company, Graph24h.GraphData> mapCompanyData) {
		this.mapCompanyData = mapCompanyData;
	}
}

