package de.bosc.fufeu.company.business;

import lotus.domino.Session;

public abstract class Activator {

	public abstract Session getSession();
	
}
