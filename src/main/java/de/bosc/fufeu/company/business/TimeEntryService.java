package de.bosc.fufeu.company.business;


import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeMap;
import java.util.Vector;

import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.NotesException;
import lotus.domino.View;

import de.bosc.fufeu.company.model.Company;
import de.bosc.fufeu.company.model.Person;
import de.bosc.fufeu.company.model.TimeEntry;

public class TimeEntryService implements Serializable{

	public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	public static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public static Integer parseDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.HOUR_OF_DAY)*60*60 + cal.get(Calendar.MINUTE)*60 + cal.get(Calendar.SECOND);
	}
	
	public static String getStringValueNow(){
		return dateFormat.format(new Date());
	}
	
	public static String parseTimeToString(Integer time){
		int seconds = time;
		int hours = (seconds - seconds % 3600)/3600;
		seconds = seconds-hours*3600;
		int minutes = (seconds - seconds % 60)/60;
		seconds = seconds-minutes*60;
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, hours);
		cal.set(Calendar.MINUTE, minutes);
		cal.set(Calendar.SECOND, seconds);
		return hours==24?"24:00:00":timeFormat.format(cal.getTime());
	}
	
	
	public static Date parseTimeEntryDate(String date, String time) throws ParseException{
		return dateTimeFormat.parse(date + " " + time);
	}
	
	
	public DatabaseService getDatabaseService() {
		return databaseService;
	}


	private DatabaseService databaseService;
	private int cutOffTime = 10*60*60; // default value 10 hours
	
	public TimeEntryService(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}

	public TimeEntry getPersonTimeEntry(Date dateTime, Person person, String type){
		TimeEntry timeEntry = createTimeEntry(dateTime, person,type);
		try {
			databaseService.debug("getPersonTimeEntry: " + dateFormat.format(dateTime) + " for person " + person.getPersonalNumber());
			View view = databaseService.getCurrentDatabase().getView("($timeentriesOpen)");
			Document docTimeEntry = view.getDocumentByKey(person.getPersonalNumber());
			
			if(docTimeEntry != null){
				databaseService.debug("getPersonTimeEntry: timeEntry found");
				timeEntry.setPerson(person);
				timeEntry = getTimeEntryFromDoc(docTimeEntry);
				timeEntry = checkTimeEntry(timeEntry, dateTime, false);
			}
		} catch (Exception e) {
			databaseService.error(e);
		}
		return timeEntry;
	}
	
	public TimeEntry checkTimeEntry(TimeEntry timeEntry,Date dateTime,boolean close) throws Exception{
		int cutOffTime = this.cutOffTime;
		StringBuffer debug = new StringBuffer("checkTimeEntry\nDate" + dateTime + "\nclose: "+ close);
		int endTime = parseDate(dateTime);
		String date = dateFormat.format(dateTime);
		
		if(timeEntry.getPerson() == null){
			timeEntry.setPerson(databaseService.getPersonService().getPersonsByPersonalNumber().get(timeEntry.getPersonalnumber()));
		}
		debug.append(" --- Timeentry person: " + timeEntry.getPerson() + "\n");
		if(timeEntry.getPerson() != null && timeEntry.getPerson().getCutOffTime() != 0){
			cutOffTime = timeEntry.getPerson().getCutOffTime();
			debug.append("\n ------ set cutOffTime from person: " + timeEntry.getPerson().getFullname() + " = " + cutOffTime + "\n");	
		}else {
			Company company = databaseService.getCompany(timeEntry.getCompany());
			if(company != null && company.getCutOffTime() != 0){
				cutOffTime = company.getCutOffTime();
				debug.append("\n ------ set cutOffTime from company: " + company.getName() + " = " + cutOffTime + "\n");	
			}
		}
		
		debug.append(" --- cutOfTime: " + cutOffTime + " --- endTime: " + endTime +"\n --- date: " + date + "\n\n");
		
		if(timeEntry.getDate().equals(date)){
			/*
			 * Fall 1: 	Ist noch gleicher Tag -> wenn close = true dann Zeiteintrag abschliessen
			 * 			mit cutOfTime Dauer
			 */
			debug.append(" --- same day\n");
			int duration = endTime - timeEntry.getStarttime();
			debug.append(" --- duration: " + duration + "\n");
			
			if(duration > cutOffTime && close){
				debug.append(" CLOSE timeEntry: endtime: " + timeEntry.getStarttime() + cutOffTime + "\n");
				timeEntry.setEndtime(timeEntry.getStarttime() + cutOffTime);
			}
		}else if(close && timeEntry.getStarttime() + cutOffTime < 24*60*60 ){
			/*
			 * Fall 2:	Ist nicht mehr gleicher Tag && close && Max.Zeitdauer erreicht
			 * 			Zeiteintrag abschiessen
			 */
			debug.append(" --- cutOfTime reached && close: CLOSE timeentry\n");
			timeEntry.setEndtime(timeEntry.getStarttime() + cutOffTime);
			saveTimeEntry(timeEntry);
			timeEntry = null;
		}else{
			/*
			 * Fall 3:	Ist nicht mehr gleicher Tag -> Zeiteintrag muss
			 * in jedem Fall abgegrenzt werden
			 */
			debug.append(" --- NOT same day\n");
			int duration = 24*60*60 - timeEntry.getStarttime();
			
			// Zeiteintrag f�r diesen Tag abschliessen
			timeEntry.setEndtime(24*60*60);
			saveTimeEntry(timeEntry);

			// Restdauer f�r maximalzeit bestimmen
			int restDuration = cutOffTime - timeEntry.getDuration();

			Calendar cal = Calendar.getInstance();
			cal.setTime(dateFormat.parse(timeEntry.getDate()));
			cal.add(Calendar.DAY_OF_MONTH, 1);
			
			// neuer Zeitentrag
			timeEntry = timeEntry.clone();
			timeEntry.setDate(dateFormat.format(cal.getTime()));
			timeEntry.setStarttime(0);
			
			if(timeEntry.getDate().equals(date)){
				/*
				 * Ist Folgetag
				 */
				if(timeEntry.getStarttime() + restDuration < endTime && close){
					/*
					 *  Fall 2a:	Maximale Zeitdauer erreicht und close = true -> Zeiteintrag abschliessen
					 * 				mit Restzeitdauer		
					 */
					timeEntry.setEndtime(timeEntry.getStarttime() + restDuration);
					saveTimeEntry(timeEntry);
					timeEntry = null;
				}
			}else{
				/*
				 * Fall 2b:	Ist auch nicht Folgetag -> in jedem Fall mit Restdauer abschliessen
				 * 			und neuen Zeiteintrag
				 */
				timeEntry.setEndtime(timeEntry.getStarttime() + restDuration);
				saveTimeEntry(timeEntry);
				timeEntry = null;
			}
		}		
		databaseService.debug(debug.toString());
		return timeEntry;
	}
	
	public TimeEntry createTimeEntry(Date dateTime, Person person, String type){
		TimeEntry timeEntry = new TimeEntry();
		timeEntry.setDate(dateFormat.format(dateTime));
		timeEntry.setOrgFunctions(person.getOrgFunctions());
		timeEntry.setPrio(person.getPrio());
		timeEntry.setPersonalnumber(person.getPersonalNumber());		
		timeEntry.setStarttime(parseDate(dateTime));
		timeEntry.setType(type);
		timeEntry.setValid(true);
		timeEntry.setCompany(person.getCompany());
		timeEntry.setQualifications(person.getQualifications());
		timeEntry.setDepartment(person.getDepartment());
		timeEntry.setPerson(person);
		
		databaseService.debug(" CREATE NEW TIMEENTRY: date:" + timeEntry.getDate() + " starttime: " + timeEntry.getStarttimeText());
		return timeEntry;
	}
	
	public TimeEntry getTimeEntryFromDoc(Document doc){
		TimeEntry timeEntry = new TimeEntry();
		try{
			timeEntry.setDocId(doc.getItemValueString("docid"));
			timeEntry.setDate(doc.getItemValueString("date"));
			timeEntry.setEndtime(doc.getItemValueInteger("endtime"));
			timeEntry.setStarttime(doc.getItemValueInteger("starttime"));
			timeEntry.setPrio(doc.getItemValueInteger("prio"));
			timeEntry.setPersonalnumber(doc.getItemValueString("personalNumber"));
			timeEntry.setType(doc.getItemValueString("type"));
			timeEntry.setOrgFunctions(new ArrayList<String>(doc.getItemValue("orgFunctions")));
			timeEntry.setQualifications(new ArrayList<String>(doc.getItemValue("qualifications")));
			timeEntry.setDepartment(doc.getItemValueString("department"));
			timeEntry.setCompany(doc.getItemValueString("company"));
			Person person = databaseService.getPersonService().getPersonsByPersonalNumber().get(timeEntry.getPersonalnumber());
			timeEntry.setPerson(person);
			timeEntry.setValid(true);
		}catch(Exception e){
			databaseService.error(e);
		}
		return timeEntry;
	}
	
	public void saveTimeEntry(TimeEntry timeEntry) throws NotesException{
		StringBuffer debug = new StringBuffer("saveTimeEntry\n");
		debug.append(" ------ date:" + timeEntry.getDate() + " starttime: " + timeEntry.getStarttimeText() + "\n");
		debug.append(" ------ docID: " + timeEntry.getDocId() + "\n");
		try{
			Document docTimeEntry = null;
			View view = databaseService.getCurrentDatabase().getView("($docid)");
			if(timeEntry.getDocId() != null){
				docTimeEntry = view.getDocumentByKey(timeEntry.getDocId());			
			}else{
				docTimeEntry = databaseService.getCurrentDatabase().createDocument();
				docTimeEntry.replaceItemValue("form", "Timeentry");
				docTimeEntry.replaceItemValue("docid", docTimeEntry.getUniversalID());
				debug.append(" ------ create new document: " + docTimeEntry.getItemValueString("docid") + "\n");
				docTimeEntry.computeWithForm(true,false);
				timeEntry.setDocId(docTimeEntry.getItemValueString("docid"));
			}
			if(docTimeEntry == null){
				databaseService.debug("docTimeEnty is NULL");
			}else{
				docTimeEntry.replaceItemValue("date", timeEntry.getDate());
				docTimeEntry.replaceItemValue("starttime", timeEntry.getStarttime());
				docTimeEntry.replaceItemValue("starttimeText", timeEntry.getStarttimeText());
				docTimeEntry.replaceItemValue("endtime",timeEntry.getEndtime());
				docTimeEntry.replaceItemValue("endtimeText",timeEntry.getEndtimeText() );
				docTimeEntry.replaceItemValue("prio",timeEntry.getPrio() );
				docTimeEntry.replaceItemValue("personalNumber",timeEntry.getPersonalnumber() );
				docTimeEntry.replaceItemValue("type",timeEntry.getType());
				docTimeEntry.replaceItemValue("orgFunctions", new Vector<String>(timeEntry.getOrgFunctions()));
				docTimeEntry.replaceItemValue("qualifications", new Vector<String>(timeEntry.getQualifications()));
				docTimeEntry.replaceItemValue("department",timeEntry.getDepartment());
				docTimeEntry.replaceItemValue("company",timeEntry.getCompany());
				
				
				docTimeEntry.save();	
				view.refresh();
				View viewOpen = databaseService.getCurrentDatabase().getView("($timeentriesOpen)");
				viewOpen.refresh();
			}
			databaseService.debug(debug.toString());	
		}catch(Exception e){
			debug.append(DatabaseService.getStackTrace(e));		
			databaseService.error(debug.toString());	
		}
	}

	public ArrayList<TimeEntry> getTimeEntriesAdhocforDay(Date date){
		
		String key = TimeEntryService.dateFormat.format(date);
		ArrayList<TimeEntry> arrTimeEnties = new ArrayList<TimeEntry>();
		try{
			View view = databaseService.getCurrentDatabase().getView("($timeentries)");
			DocumentCollection coll = view.getAllDocumentsByKey(key,true); 
			Document doc = coll.getFirstDocument();
			databaseService.debug("TimeEntryService.getTimeEntriesAdhocforDay: coll=" + coll.getCount() + " date: " + key );
			while(doc!=null){
				Document docNext = coll.getNextDocument(doc);					
				TimeEntry timeEntry = getTimeEntryFromDoc(doc);
				if(timeEntry.getType().equals("ADHOC")){
					arrTimeEnties.add(timeEntry);
				}
				doc.recycle();
				doc = docNext;
			}
		}catch(Exception e){
			databaseService.error(e);
		}
		return arrTimeEnties;
	}
	
	public int getCutOfTime() {
		return cutOffTime;
	}

	public void setCutOfTime(int cutOfTime) {
		this.cutOffTime = cutOfTime;
	}

	
}
