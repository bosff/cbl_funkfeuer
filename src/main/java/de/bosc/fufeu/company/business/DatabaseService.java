package de.bosc.fufeu.company.business;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.bosc.fufeu.company.model.Company;
import de.bosc.fufeu.company.model.DatabaseConfig;
import de.bosc.fufeu.company.model.Listentry;
import de.bosc.fufeu.company.model.QualificationGroup;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.Log;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;

public class DatabaseService implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String ORGFUNCTION = "ORGFUNCTION";
	public static final String QUALIFICATION = "QUALIFICATION";
	public static final String SUMMARYORGANISATION = "SUMMARYORGANISATION";
	
	public static String getStackTrace(Throwable t) {
		   StringWriter sw = new StringWriter();
	    t.printStackTrace(new PrintWriter(sw));
	    return sw.toString();
	}
	
	private LinkedHashMap <String,Company> companies;
	private HashMap<String,TreeMap<String,Listentry>> listEntries;
	private Map<Integer,QualificationGroup> mapQualificationGroupsGraph24;
	private Map<Integer,QualificationGroup> mapQualificationGroupsSummary;
	private Map<Integer,QualificationGroup> mapQualificationGroupsWerkschutz;

	private Map<String, QualificationGroup> mapQualificationGroups;
	private DatabaseConfig databaseConfig;
	private PersonService personService;
	private TimeEntryService timeEntryService;
	private SummaryService summaryService;
	private StandbyService standbyService;
	private StandbyCalendarService standbyCalendarService;
	private Graph24hService graph24hService;
	private QualificationGroupService qualificationGroupService;
	private Integer logLevel = 3;
	protected Activator activator;

	public DatabaseService() {
		
	}
	
	public Map<Integer, QualificationGroup> getMapQualificationGroupsSummary() {
		if(this.mapQualificationGroupsSummary == null) {
			this.mapQualificationGroupsSummary = new TreeMap<Integer, QualificationGroup>();
			for(String docid:this.getDatabaseConfig().getQualificationsSummary()) {
				QualificationGroup qualificationGroup = this.getMapQualificationGroups().get(docid);
				if(qualificationGroup != null) {
					this.mapQualificationGroupsSummary.put(qualificationGroup.getSort(), qualificationGroup);
				}
			}
			info("----------- mapQualificationGroupsSummary: " + this.mapQualificationGroupsSummary.values());

		}
		return mapQualificationGroupsSummary;
	}

	public Map<Integer, QualificationGroup> getMapQualificationGroupsWerkschutz() {
		if(this.mapQualificationGroupsWerkschutz == null) {
			this.mapQualificationGroupsWerkschutz = new TreeMap<Integer, QualificationGroup>();
			for(String docid:this.getDatabaseConfig().getQualificationsWerkschutz()) {
				QualificationGroup qualificationGroup = this.getMapQualificationGroups().get(docid);
				if(qualificationGroup != null) {
					this.mapQualificationGroupsWerkschutz.put(qualificationGroup.getSort(), qualificationGroup);
				}
			}
			info("----------- mapQualificationGroupsWerkschutz: " + this.mapQualificationGroupsWerkschutz.values());

		}
		return mapQualificationGroupsWerkschutz;
	}

	public Map<String, QualificationGroup> getMapQualificationGroups() {
		if(this.mapQualificationGroups == null) {
			this.mapQualificationGroups = this.getQualificationGroupService().getMapQualificationGroups();
		}
		return mapQualificationGroups;
	}

	public void setMapQualificationGroups(Map<String, QualificationGroup> mapQualificationGroups) {
		this.mapQualificationGroups = mapQualificationGroups;
	}

	public Integer getLogLevel() {
		return logLevel;
	}

	public void debug(String msg){
		log("DEBUG:" + msg,4);
	}

	public void error(Exception e){		
		log("ERROR:" + getStackTrace(e),true);
	}

	public void error(String msg){
		log("ERROR: " + msg,true);
	}

	public Activator getActivator() {
		return activator;
	}

	public List<String> getCleanedListQualifications(List<String> originalList){
		List<String> cleanedList = new ArrayList<String>();
		cleanedList.addAll(originalList);
		Map<String,Listentry> listentries = getQualifications();
		if(originalList != null){
			for(String key:originalList){
				Listentry listentry = listentries.get(key);
				cleanedList.removeAll(listentry.getIncludedListentries());
			}			
		}
		return cleanedList;
	}

	public LinkedHashMap<String, Company> getCompanies() {
		return companies;
	}

	public Company getCompany(String key){
		return getCompanyList().get(key);
	}

	public LinkedHashMap<String,Company> getCompanyList(){
		if(companies == null){
			companies = new LinkedHashMap<String,Company>();
			try {
				View view = getCurrentDatabase().getView("($companies)");
				Document doc = view.getFirstDocument();
				while(doc != null){
					Document docNext = view.getNextDocument(doc);
					Company company = new Company();
					company.setName(doc.getItemValueString("companyName"));
					company.setCutOffTime(doc.getItemValueInteger("cutOffTime"));
					company.setSort(doc.getItemValueInteger("sort"));
					company.setAcitive(doc.getItemValueString("active").equals("1"));
					company.setDocId(doc.getItemValueString("docID"));
					company.setColor(doc.getItemValueString("color"));
					company.setTextColor(doc.getItemValueString("textcolor"));
					companies.put(company.getDocId(), company);
					doc.recycle();
					doc = docNext;
				}
				view.recycle();
			} catch (NotesException e) {
				error(e);
			}
		}		
		return companies;
	}

	public Database getCurrentDatabase(){
		Database db = null;
		try{
		    Session session = this.activator.getSession();
		    db = session.getCurrentDatabase();			
		}catch(Exception e){
			error(e);
		}
		return db;
	}

	@SuppressWarnings("unchecked")
	public DatabaseConfig getDatabaseConfig(){
		try{
			if (databaseConfig == null){
				View view = getCurrentDatabase().getView("($databaseConfig)");
				Document docProfile = view.getFirstDocument();
				databaseConfig = new DatabaseConfig();
				info("############## LOGGING: " + databaseConfig.isLogging() + " ##################");
				if(docProfile != null){
					databaseConfig.getSummaryOrgFunctions().addAll(docProfile.getItemValue("SummaryOrgFunctions"));
					info("----------- " + databaseConfig.getSummaryOrgFunctions());
					databaseConfig.setLogging(docProfile.getItemValueString("logging").equals("1"));
					databaseConfig.setLogLevel(docProfile.getItemValueInteger("LogLevel"));
					this.logLevel = databaseConfig.getLogLevel();
					info("----------- LOG Level" + databaseConfig.getLogLevel());				
					databaseConfig.setHttpServername(docProfile.getItemValueString("httpServername"));
					info("----------- httpServername: " + databaseConfig.getHttpServername());					
					databaseConfig.setQualificationsGraph24(new ArrayList<String>(docProfile.getItemValue("QualificationsGraph24")));
					info("----------- QualificationsGraph24: " + databaseConfig.getQualificationsGraph24());					
					databaseConfig.setQualificationsSummary(new ArrayList<String>(docProfile.getItemValue("QualificationsSummary")));
					info("----------- QualificationsSummary: " + databaseConfig.getQualificationsSummary());					
					databaseConfig.setQualificationsWerkschutz(new ArrayList<String>(docProfile.getItemValue("QualificationsWerkschutz")));
					info("----------- QualificationsWerkschutz: " + databaseConfig.getQualificationsWerkschutz());					
					Integer graph24hMinimumPresence = docProfile.getItemValueInteger("graph24hMinimumPresence")*60;
					if(graph24hMinimumPresence != 0){
						databaseConfig.setGraph24hMinimumPresence(graph24hMinimumPresence);
					}
				}
				
			}
			//databaseConfig.setLogging(true);
		}catch(Exception e){
			error(e);
		}

		return databaseConfig;
	}
	
	public Graph24hService getGraph24hService() {
		if(this.graph24hService == null) {
			this.graph24hService = new Graph24hService(this);
		}
		return graph24hService;
	}
	
	public Map<String,TreeMap<String,Listentry>> getListEntries(){
		if(listEntries == null){
			listEntries = new HashMap<String,TreeMap<String,Listentry>>();
			try {
				View view = getCurrentDatabase().getView("($listentries)");
				Document doc = view.getFirstDocument();
				while(doc != null){
					Document docNext = view.getNextDocument(doc);
					String type = doc.getItemValueString("type");
					TreeMap<String,Listentry> list = listEntries.get(type);
					if(list == null){
						list = new TreeMap<String,Listentry>();
						listEntries.put(type,list);
					}
					Listentry listentry = new Listentry();
					listentry.setDocId(doc.getItemValueString("docid"));
					listentry.setAlias(doc.getItemValueString("alias"));
					listentry.setName(doc.getItemValueString("entryname"));
					listentry.setDisplay(doc.getItemValueString("display"));
					listentry.setSort(doc.getItemValueInteger("sort"));
					listentry.setStyle(doc.getItemValueString("style"));
					listentry.setXPageEditable(doc.getItemValueString("xPageEditable").equals("1"));
					listentry.setIncludedListentries(doc.getItemValue("includedListentries"));

					if(listentry.getStyle().equals("")){
						listentry.setStyle("padding-top: 10px; border-bottom-width: 1px;border-bottom-style: solid;border-bottom-color: rgb(1,176,240);line-height:22px;text-align:left; background-color: rgba(0,0,0,0.15);");
					}
					
					list.put(listentry.getDocId(), listentry);
					doc.recycle();
					doc = docNext;
				}
				view.recycle();
			} catch (NotesException e) {
				error(e);
			}
		}		
		return listEntries;
	}
	
	public Map<Integer, QualificationGroup> getMapQualificationGroupsGraph24() {
		if(this.mapQualificationGroupsGraph24 == null) {
			this.mapQualificationGroupsGraph24 = new TreeMap<Integer, QualificationGroup>();
			for(String docid:this.getDatabaseConfig().getQualificationsGraph24()) {
				QualificationGroup qualificationGroup = this.getMapQualificationGroups().get(docid);
				if(qualificationGroup != null) {
					this.mapQualificationGroupsGraph24.put(qualificationGroup.getSort(), qualificationGroup);
				}
			}
			info("----------- mapQualificationGroupsGraph24: " + this.mapQualificationGroupsGraph24.values());

		}
		return mapQualificationGroupsGraph24;
	}
	
	public Map<String,Listentry> getOrgFuncions(){
		return getListEntries().get(DatabaseService.ORGFUNCTION);
	}
	
	public List<Listentry> getOrgFuncionsXPageEditable(){
		List<Listentry> list = new ArrayList<Listentry>();
		for(Listentry listentry:getOrgFuncions().values()){
			if(listentry.isXPageEditable()){
				list.add(listentry);
			}
		}
		return list;
	}
	
	public PersonService getPersonService() {
		if(personService == null){
			personService = new PersonService(this);
		}
		return personService;
	}
	
	public QualificationGroupService getQualificationGroupService() {
		if(this.qualificationGroupService == null) {
			this.qualificationGroupService = new QualificationGroupService(this);
		}
		return qualificationGroupService;
	}

	public Map<String,Listentry> getQualifications(){
		return getListEntries().get(DatabaseService.QUALIFICATION);
	}

	public List<Listentry> getQualificationsXPageEditable(){
		List<Listentry> list = new ArrayList<Listentry>();
		for(Listentry listentry:getQualifications().values()){
			if(listentry.isXPageEditable()){
				list.add(listentry);
			}
		}
		return list;
	}

	public StandbyCalendarService getStandbyCalendarService() {
		if(this.standbyCalendarService == null) {
			this.standbyCalendarService = new StandbyCalendarService(this);
		}
		return standbyCalendarService;
	}
	
	public StandbyService getStandbyService() {
		if(standbyService == null) {
			this.standbyService = new StandbyService(this);
		}
		return standbyService;
	}
	
	public SummaryService getSummaryService() {
		if(this.summaryService == null) {
			this.summaryService = new SummaryService(this);
		}
		return summaryService;
	}	
	
	public TimeEntryService getTimeEntryService() {
		if(this.timeEntryService == null) {
			this.timeEntryService = new TimeEntryService(this);
		}
		return timeEntryService;
	}
	
	public  Log getXPagesLog(){
		Log xPagesLog = null;
		try{
			if(this.activator == null) {
				System.out.println("DATABASESERVICE !!!!! ACTIVATOR IS NULL");
			}else {
				Session session = this.activator.getSession();
				Database db = this.getCurrentDatabase();
				xPagesLog = session.createLog(new SimpleDateFormat("HH:mm:ss").format(new Date()) +  " - XPages of database: " + db.getTitle());
				xPagesLog.openNotesLog(db.getServer(), db.getFilePath().replace(db.getFileName(),"alog.nsf" ));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return xPagesLog;
	}
	public void info(String msg){
		log("INFO:" + msg,3);
	}
	private void log(String msg, boolean isError){
		
		try {
			Log xPagesLog = getXPagesLog();
			if(xPagesLog != null){
				try {
					if(isError){
						xPagesLog.logError(10000,msg);
					}else{				
						xPagesLog.logAction(msg);
					}
				} catch (NotesException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println(msg);
			}
		} catch (Exception e) {
			System.out.println(msg);
			e.printStackTrace();
		}
		
	}
	private void log(String msg,Integer level){
		if(level <= this.logLevel){
			log(msg,false);
		}
	}
	
	
	public void setActivator(Activator activator) {
		System.out.println("DatabaseService setActivator");
		this.activator = activator;
	}


	public void verbose(String msg){
		log("VERBOSE:" + msg,5);
	}
	
	
	public void warning(String msg){
		log("WARNING:" + msg,2);
	}
}
