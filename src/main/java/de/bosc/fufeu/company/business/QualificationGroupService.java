package de.bosc.fufeu.company.business;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import de.bosc.fufeu.company.model.QualificationGroup;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;

public class QualificationGroupService  implements Serializable {

	private DatabaseService databaseService;
	private Map<String,QualificationGroup> mapQualificationGroups;

	public QualificationGroupService(DatabaseService databaseService) {
		this.databaseService = databaseService;
		try {
			this.mapQualificationGroups = new HashMap<String, QualificationGroup>();
			View view = this.databaseService.getCurrentDatabase().getView("($qualificationGroups)");
			Document doc = view.getFirstDocument();
			while(doc != null) {
				Document docNext = view.getNextDocument(doc);
				QualificationGroup qualificationGroup = new QualificationGroup();
				qualificationGroup.setDisplay(doc.getItemValueString("display"));
				qualificationGroup.setName(doc.getItemValueString("name"));
				qualificationGroup.setAlias(doc.getItemValueString("alias"));
				qualificationGroup.setSort(doc.getItemValueInteger("sort"));
				qualificationGroup.setList(doc.getItemValue("list"));
				qualificationGroup.setStyle(doc.getItemValueString("style"));
				qualificationGroup.setDocId(doc.getItemValueString("docID"));
				this.mapQualificationGroups.put(qualificationGroup.getDocId(), qualificationGroup);
				doc.recycle();
				doc = docNext;
			}
			databaseService.debug("QualificationGroupService: " + this.mapQualificationGroups);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Map<String, QualificationGroup> getMapQualificationGroups() {
		return mapQualificationGroups;
	}

}
