package de.bosc.fufeu.company.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import de.bosc.fufeu.company.model.Company;
import de.bosc.fufeu.company.model.Listentry;
import de.bosc.fufeu.company.model.Person;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;

public class PersonService implements Serializable{

	private static final long serialVersionUID = 1L;

	private Person currentPerson;
	private DatabaseService databaseService;
	private HashMap<String,Person> persons = new HashMap<String, Person>();
	private HashMap<String,Person> personsByPersonalNumber = new HashMap<String, Person>();
	private TreeMap<String,Person> personsByLastname = new TreeMap<String, Person>();
	
	public PersonService(DatabaseService databaseService) {
		this.databaseService = databaseService;
		initCurrentPerson();
	}

	public void initCurrentPerson(){
		if(currentPerson == null){
			StringBuilder sb = new StringBuilder();
			currentPerson = new Person();
			try {			
				Session session = databaseService.getActivator().getSession();
				Database db = databaseService.getCurrentDatabase();
				View view = db.getView("($personByShortname)");
				
				Document docPerson = view.getFirstDocument();
				while(docPerson != null){
					Document docNext = view.getNextDocument(docPerson);
					Person person = createPersonFromDoc(docPerson);
					persons.put(person.getShortname(), person);
					personsByPersonalNumber.put(person.getPersonalNumber(), person);
					personsByLastname.put(person.getFullname(), person);
					sb.append("\n" + person.getPersonalNumber() + ": " + person.getShortname());
					docPerson.recycle();
					docPerson = docNext;
				}
				sb.append("\n----------------------------------------------" );
				currentPerson = persons.get(session.getEffectiveUserName());
				sb.append("\n" + persons.containsKey(session.getEffectiveUserName()) + ": " + session.getEffectiveUserName());
				if(currentPerson == null){
					Person person = new Person();
					person.setLastname("N.N.");
					person.setFirstname("N.N.");
					currentPerson = person;
				}
				databaseService.debug("PersonService.getCurrentPerson: " + session.getUserNameList() + " --- " + session.getEffectiveUserName() + " ---- " + (currentPerson != null) + "\n" + sb.toString());
			} catch (NotesException e) {
				databaseService.error(e);
			}			
		}		
	}

	public Person createPersonFromDoc(Document docPerson){
		Person person = new Person();
		person.setLastname("N.N.");
		person.setFirstname("N.N.");

		try{
			if(docPerson != null){
				person.setUnid(docPerson.getUniversalID());
				person.setLastname(docPerson.getItemValueString("Lastname"));
				person.setFirstname(docPerson.getItemValueString("Firstname"));
				person.setDepartment(docPerson.getItemValueString("department"));
				person.setCompany(docPerson.getItemValueString("company"));
				person.setCutOffTime(docPerson.getItemValueInteger("cutOffTime"));
				person.setPrio(docPerson.getItemValueInteger("Prio"));
				person.setPersonalNumber(docPerson.getItemValueString("PersonalNumber"));
				person.setOrgFunctions(docPerson.getItemValue("orgFunctions"));
				person.setQualifications(docPerson.getItemValue("qualifications"));
				person.setColor(docPerson.getItemValueString("color"));
				person.setShortname(docPerson.getItemValueString("shortname"));
				person.setPhoneNumber(docPerson.getItemValueString("phoneNumber"));
				person.setPhoneNumberBusiness(docPerson.getItemValueString("phoneNumberBusiness"));
				person.setMobileNumber(docPerson.getItemValueString("mobileNumber"));
				person.setActive(docPerson.getItemValueString("active").equals("1"));
				person.setValid(true);
			}
		}catch(Exception e){
			databaseService.error(e);
		}
		return person;
	}
	
	public void savePersonQualification(Person person){
		try{			
			if(person != null){
				Database db = databaseService.getCurrentDatabase();
				Document docPerson = db.getDocumentByUNID(person.getUnid());
				if(docPerson != null){
					Vector<String> quals = new Vector<String>();
					quals.addAll(person.getQualifications());
					docPerson.replaceItemValue("qualifications",quals);
					docPerson.save();
				}
			}
		
		}catch(Exception e){
			databaseService.error(e);
		}
		
	}

	public Person getCurrentPerson() {
		return currentPerson;
	}
	
	public String getPersonCompanyName(Person person){
		String strCompany = "";
		if(person != null && person.getCompany() != null){
			Company company = databaseService.getCompanyList().get(person.getCompany());
			strCompany = (company == null?"-----":company.getName());
		}
		return strCompany;
	}

	public List<String> getPersonQualificationNames(Person person){
		List<String> qualifications = new ArrayList<String>();
		
		if(person != null && person.getQualifications() != null){
			for(String id:person.getQualifications()){
				Listentry listentry = databaseService.getQualifications().get(id);
				if(listentry != null){
					qualifications.add(listentry.getAlias());
				}else{
					qualifications.add("n.n.");
				}
				
			}
		}
		return qualifications;
	}
	
	public List<String> getCleanedPersonQualificationNames(Person person){
		List<String> qualifications = new ArrayList<String>();
		
		if(person != null && person.getQualifications() != null){
			for(String id:databaseService.getCleanedListQualifications(person.getQualifications())){
				Listentry listentry = databaseService.getQualifications().get(id);
				if(listentry != null){
					qualifications.add(listentry.getAlias());
				}else{
					qualifications.add("n.n.");
				}
				
			}
		}
		return qualifications;
	}
	
	public HashMap<String, Person> getPersons() {
		return persons;
	}
	public ArrayList<String> getArrayPersons() {
		ArrayList<String> arrPersons = new ArrayList<String>();
		for(Person person:persons.values()){
			arrPersons.add(person.getLastname() + ", " + person.getFirstname() + "|" + person.getShortname());
		}
		return arrPersons;
	}
	
	public List<String> getArrayPersonsOrgFunction() {
		List<String> arrPersons = new ArrayList<String>();
		String orgFunction = this.databaseService.getStandbyCalendarService().getStandbyCalendar().getCurrentStandby().getOrgFunction();
		if(orgFunction == null || orgFunction.equals("")) {
			orgFunction = "AEAC16E024398A9DC12581520050A42A";
		}
		databaseService.info("orgFunction: " + orgFunction);
		for(Person person:persons.values()){
			if(person.getOrgFunctions().contains(orgFunction)){
				arrPersons.add(person.getLastname() + ", " + person.getFirstname() + "|" + person.getShortname());				
			}
		}
		databaseService.info("getArrayPersonsOrgFunction: " + arrPersons.size() + " -- " + arrPersons);
		return arrPersons;
	}
	
	public void setPersons(HashMap<String, Person> persons) {
		this.persons = persons;
	}
	public void setCurrentPerson(Person currentPerson) {
		this.currentPerson = currentPerson;
	}

	public HashMap<String, Person> getPersonsByPersonalNumber() {
		return personsByPersonalNumber;
	}

	public void setPersonsByPersonalNumber(
			HashMap<String, Person> personsByPersonalNumber) {
		this.personsByPersonalNumber = personsByPersonalNumber;
	}

	public TreeMap<String, Person> getPersonsByLastname() {
		return personsByLastname;
	}
	
	public ArrayList<Person> getActivePersonsByLastname() {
		ArrayList<Person> persons = new ArrayList<Person>();
		for(Person person:getPersonsByLastname().values()){
			if(person.isActive()){
				persons.add(person);				
			}
		}
		return persons;
	}
	
	public void removeQualification(Person person, String qualificationDocid){
		databaseService.debug(" remove qualification: " + qualificationDocid);
		person.getQualifications().remove(qualificationDocid);
		savePersonQualification(person);
	}
	
	public void addQualification(Person person, String qualificationDocid){
		databaseService.debug(" add qualification: " + qualificationDocid);
		person.getQualifications().add(qualificationDocid);
		savePersonQualification(person);
	}
}
