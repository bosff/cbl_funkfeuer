package de.bosc.fufeu.company.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.bosc.fufeu.company.model.Listentry;
import de.bosc.fufeu.company.model.Person;
import de.bosc.fufeu.company.model.QualificationGroup;
import de.bosc.fufeu.company.model.Standby;
import de.bosc.fufeu.company.model.SummaryOrgFunction;
import de.bosc.fufeu.company.model.SummaryQualificationGroup;
import de.bosc.fufeu.company.model.SummaryStandby;
import de.bosc.fufeu.company.model.TimeEntry;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.View;

public class SummaryService implements Serializable{
	
	private DatabaseService databaseService;
	
	public SummaryService(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}

	
	
	
	public List<SummaryOrgFunction> getListOrgFunctionPersons(List<Person> actualPersons, List<SummaryStandby> listSummaryStandby){
		List<SummaryOrgFunction> arrPersons = new ArrayList<SummaryOrgFunction>();
		List<Person> usedPersons = new ArrayList<Person>();
		List<String> orgFunctions = databaseService.getDatabaseConfig().getSummaryOrgFunctions();
		
		Map<String, Listentry> listEntries = databaseService.getListEntries().get(DatabaseService.ORGFUNCTION);
		
		for(SummaryStandby summaryStandby:listSummaryStandby){
			if(summaryStandby.getTimeEntry() != null && summaryStandby.getTimeEntry().getPerson() != null){
				usedPersons.add(summaryStandby.getTimeEntry().getPerson());
			}			
		}
		
		
		for(String function:orgFunctions){
			databaseService.debug(" #########--- function: " + function);
			SummaryOrgFunction summaryOrgFunction = new SummaryOrgFunction();
			Listentry listentry = listEntries.get(function);
			if(listentry == null){
				listentry = new Listentry();
				listentry.setName("entry not found");
				listentry.setAlias("entry not found");
				listentry.setDisplay("entry not found");
				listentry.setSort(new Integer(0));
				databaseService.debug(" --------- function not found: " + function);
			}

			summaryOrgFunction.setOrgFunction(listentry);
			
			for(Person person:actualPersons){
				if(!usedPersons.contains(person)){
					if(summaryOrgFunction.getPerson() == null && person.getOrgFunctions().contains(function)){
						databaseService.debug(" --------- person: " + person + " --- contains " + function);
						summaryOrgFunction.setPerson(person);
						usedPersons.add(person);
					}
				}
			}
			if(summaryOrgFunction.getPerson() == null){
				Person person = new Person();
				person.setLastname("n.n.");
				person.setFirstname("n.n.");
				summaryOrgFunction.setPerson(person);
			}
			arrPersons.add(summaryOrgFunction);
			
			databaseService.debug(" ################## SummaryService.getListOrgFunctionPersons: " + summaryOrgFunction.getOrgFunction().getDisplay());
		}
		return arrPersons;
	}

	
	
	public Collection<TimeEntry> sortTimeEntriesByPrioPerson(List<TimeEntry> timeEntries){
		Map<Integer,TimeEntry> mapTimeEntries = new TreeMap<Integer, TimeEntry>();
		int i = 0;
		
		for(TimeEntry timeEntry:timeEntries){
			mapTimeEntries.put(new Integer((-1)*timeEntry.getPrio()*10000 + i), timeEntry);
			i++;
		}
		
		return mapTimeEntries.values();
	}
	
	public List<SummaryStandby> getListStandbys(Date date){
		List<SummaryStandby>  listStandbyTimeEntries = new ArrayList<SummaryStandby>();
		try{
			int currentTime = TimeEntryService.parseDate(date);
			for(Standby standby:this.databaseService.getStandbyService().getListStandby()){		
				View view = databaseService.getCurrentDatabase().getView("($timeentries)");
				DocumentCollection coll = view.getAllDocumentsByKey(TimeEntryService.dateFormat.format(date),true); 
				Document doc = coll.getFirstDocument();
				SummaryStandby summaryStandby = new SummaryStandby();
				summaryStandby.setStandby(standby);
				while(doc != null){
					Document docNext = coll.getNextDocument(doc);					
					TimeEntry timeEntry = this.databaseService.getTimeEntryService().getTimeEntryFromDoc(doc);
					if(timeEntry.getType().equals(standby.getDocid())){
						if(currentTime > timeEntry.getStarttime() && currentTime < timeEntry.getEndtime()){
							if(!timeEntry.getPersonalnumber().equals("")){					
								Person person = databaseService.getPersonService().getPersonsByPersonalNumber().get(timeEntry.getPersonalnumber());
								timeEntry.setPerson(person);
							}else{
								Person person = new Person();
								person.setLastname("n.n.");
								person.setFirstname("n.n.");
								timeEntry.setPerson(person);
							}
							summaryStandby.setTimeEntry(timeEntry);
						}
					}
					doc.recycle();
					doc = docNext;
				}
				if(summaryStandby.getTimeEntry() == null){
					TimeEntry timeEntryTemp = new TimeEntry();
					Person person = new Person();
					person.setLastname("n.n.");
					person.setFirstname("n.n.");
					timeEntryTemp.setPerson(person);
					summaryStandby.setTimeEntry(timeEntryTemp);
				}
				listStandbyTimeEntries.add(summaryStandby);							
			}			
		}catch(Exception e){
			databaseService.error(e);
		}
		return listStandbyTimeEntries;
	}
	
	public List<SummaryQualificationGroup> getSummaryQualificationGroups(){
		return this.getSummaryQualificationGroups(new Date());
	}
	
	public List<SummaryQualificationGroup> getSummaryQualificationGroups(Date date){
		List<SummaryQualificationGroup> listSummaryQualificationGroups = new ArrayList<SummaryQualificationGroup>();
		List<Person> listActualPersons = this.getListActualPersons(date);
		Map<Integer, QualificationGroup> mapQualificationGroupsSummary = this.databaseService.getMapQualificationGroupsSummary();
		int j = 1;
		for(QualificationGroup qualificationGroup:mapQualificationGroupsSummary.values()) {
			Map<String,Person> listPersons = new TreeMap<String,Person>();
			for(Person person:listActualPersons) {
				List<String> listQaulis = new ArrayList<String>(person.getQualifications());
				listQaulis.retainAll(qualificationGroup.getList());
				if(listQaulis.size() > 0) {
					listPersons.put(person.getFullname() + j,person);
					j++;
				}
			}
			SummaryQualificationGroup summaryQualificationGroup = new SummaryQualificationGroup();
			summaryQualificationGroup.setListPersons(listPersons);
			summaryQualificationGroup.setQualificationGroup(qualificationGroup);
			listSummaryQualificationGroups.add(summaryQualificationGroup);
		}
		return listSummaryQualificationGroups;
	}
	
	public List<SummaryQualificationGroup> getWerkschutzQualificationGroups(){
		return this.getWerkschutzQualificationGroups(new Date());
	}
	
	public List<SummaryQualificationGroup> getWerkschutzQualificationGroups(Date date){
		List<SummaryQualificationGroup> listSummaryQualificationGroups = new ArrayList<SummaryQualificationGroup>();
		try {
			List<Person> listActualPersons = this.getListActualPersons(date);
			Map<Integer, QualificationGroup> mapQualificationGroupsSummary = this.databaseService.getMapQualificationGroupsWerkschutz();
			int j = 1;
			this.databaseService.info("mapQualificationGroupsSummary: " + mapQualificationGroupsSummary.size());
			for(QualificationGroup qualificationGroup:mapQualificationGroupsSummary.values()) {
				Map<String,Person> listPersons = new TreeMap<String,Person>();
				for(Person person:listActualPersons) {
					List<String> listQaulis = new ArrayList<String>(person.getQualifications());
					listQaulis.retainAll(qualificationGroup.getList());
					if(listQaulis.size() > 0) {
						listPersons.put(person.getFullname() + j,person);
						j++;
					}
				}
				SummaryQualificationGroup summaryQualificationGroup = new SummaryQualificationGroup();
				summaryQualificationGroup.setListPersons(listPersons);
				summaryQualificationGroup.setQualificationGroup(qualificationGroup);
				listSummaryQualificationGroups.add(summaryQualificationGroup);
			}
		} catch (Exception e) {
			this.databaseService.error(e);
		}
		return listSummaryQualificationGroups;
	}
	
	public List<Person> getListActualPersons(Date date){
		Map<Integer,Person> listPersons  = new TreeMap<Integer, Person>();
		List<Person> arrPersons = new ArrayList<Person>();
		try{
			int currentTime = TimeEntryService.parseDate(date);

			View view = databaseService.getCurrentDatabase().getView("($timeentries)");
			DocumentCollection coll = view.getAllDocumentsByKey(TimeEntryService.dateFormat.format(date),true); 
			Document doc = coll.getFirstDocument();
			databaseService.debug("SummaryService.getListActualPersons: coll=" + coll.getCount() + " currentTime: " + currentTime );
			int j = 1;
			while(doc!=null){
				Document docNext = coll.getNextDocument(doc);					
				TimeEntry timeEntry = this.databaseService.getTimeEntryService().getTimeEntryFromDoc(doc);
				if(timeEntry.getType().equals("ADHOC")){
					if(timeEntry.getStarttime() < currentTime){
						ArrayList<Person> arrPersonTemp = new ArrayList<Person>();
						int endtime = timeEntry.getEndtime();
						if(endtime == 0 || endtime == -1 || endtime > currentTime){
							Person person = databaseService.getPersonService().getPersonsByPersonalNumber().get(timeEntry.getPersonalnumber());
							if(person != null){
								listPersons.put(person.getPrio()*1000 + j,person);
								j++;
							}
						}
					}
				}
				doc.recycle();
				doc = docNext;
			}
			databaseService.debug("SummaryService.getListActualPersons: actual persons = " + listPersons.values().size());
			List<Person> arrPersonTemp = new ArrayList<Person>();
			for(Person person:listPersons.values()){
				arrPersonTemp.add(person);
			}
			
			for(int i = arrPersonTemp.size();i>0;i--){
				arrPersons.add(arrPersonTemp.get(i-1));
			}
		}catch(Exception e){
			databaseService.error(e);
		}
		return arrPersons;
	}
}
