package de.bosc.fufeu.company.test;

import de.bosc.fufeu.company.business.DatabaseService;
import lotus.domino.Database;
import lotus.domino.Session;

public class TestDatabaseService extends DatabaseService{

	public TestDatabaseService() {
		super();
		this.activator = new TestActivator();
	}

	@Override
	public Database getCurrentDatabase() {
		Database db = null;
		try{
		    Session session = this.activator.getSession();
		    db = session.getDatabase("","CBL/FuFeuCBLTest.nsf");			
		}catch(Exception e){
			e.printStackTrace();
		}
		return db;
	}

}
