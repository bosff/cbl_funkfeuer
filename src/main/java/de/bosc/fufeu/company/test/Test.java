package de.bosc.fufeu.company.test;

import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import de.bosc.fufeu.company.model.Company;
import de.bosc.fufeu.company.model.Graph24h.GraphData;
import de.bosc.fufeu.company.model.QualificationGroup;

public class Test {

	public static void main(String[] args) {
		TestDatabaseService testDatabaseService = new TestDatabaseService();
		testDatabaseService.getDatabaseConfig();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE,-7);
		testDatabaseService.getGraph24hService().setSelectedDate(cal.getTime());
		String svgGraphic24h = testDatabaseService.getGraph24hService().getSvgGraphic24h();
		Map<Integer, QualificationGroup> mapQualificationGroupsGraph24 = testDatabaseService.getMapQualificationGroupsGraph24();
		System.out.println("" + mapQualificationGroupsGraph24.keySet());
	}

}
