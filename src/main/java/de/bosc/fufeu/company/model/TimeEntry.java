package de.bosc.fufeu.company.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.bosc.fufeu.company.business.TimeEntryService;

public class TimeEntry implements Serializable{

	private String docId;
	private String company;
	private String department;
	private String personalnumber;
	private Integer starttime = -1;
	private String starttimeText = "";
	private Integer endtime = -1;
	private String endtimeText = "";
	private String date = "";
	private Integer prio = 0;
	private Integer duration = 0;
	private String type = "";
	private List<String> orgFunctions = new ArrayList<String>();
	private List<String> qualifications = new ArrayList<String>();
	private boolean valid = false;
	private Person person;
	
	public String getPersonalnumber() {
		return personalnumber;
	}
	public void setPersonalnumber(String personalnumber) {
		this.personalnumber = personalnumber;
	}
	public Integer getStarttime() {
		return starttime;
	}
	public void setStarttime(Integer starttime) {
		this.starttime = starttime;
		this.starttimeText = starttime == -1?"":TimeEntryService.parseTimeToString(starttime);
	}	
	public Integer getEndtime() {
		return endtime;
	}
	public void setEndtime(Integer endtime) {
		this.endtime = endtime;
		this.endtimeText = endtime == -1?"":TimeEntryService.parseTimeToString(endtime);
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Integer getPrio() {
		return prio;
	}
	public void setPrio(Integer prio) {
		this.prio = prio;
	}
	public List<String> getOrgFunctions() {
		if(orgFunctions == null || orgFunctions.size() == 0){
			orgFunctions = new ArrayList<String>();
			orgFunctions.add("");
		}
		return orgFunctions;
	}
	public void setOrgFunctions(List<String> orgFunctions) {
		this.orgFunctions = orgFunctions;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public String getStarttimeText() {
		return starttimeText;
	}
	public void setStarttimeText(String starttimeText) {
		this.starttimeText = starttimeText;
	}
	public String getEndtimeText() {
		return endtimeText;
	}
	public void setEndtimeText(String endtimeText) {
		this.endtimeText = endtimeText;
	}
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}	
	public Integer getDuration() {
		return duration;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	private void calculateDuration(){
		if(starttime == -1 || endtime == -1){
			duration = 0;
		}else{
			duration = endtime - starttime;
		}
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public List<String> getQualifications() {
		return qualifications;
	}
	public void setQualifications(List<String> qualifications) {
		this.qualifications = qualifications;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public TimeEntry clone(){
		TimeEntry timeEntry = new TimeEntry();
		timeEntry.setDate(date);
		timeEntry.setStarttime(-1);
		timeEntry.setEndtime(-1);
		timeEntry.setType(type);
		timeEntry.setOrgFunctions(orgFunctions);
		timeEntry.setPersonalnumber(personalnumber);
		timeEntry.setPrio(new Integer(prio));
		timeEntry.setPerson(person);
		timeEntry.setCompany(company);
		timeEntry.setDepartment(department);
		timeEntry.setQualifications(qualifications);
		return timeEntry;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	
}
