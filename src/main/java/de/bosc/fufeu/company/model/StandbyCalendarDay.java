package de.bosc.fufeu.company.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.bosc.fufeu.company.business.DatabaseService;

public class StandbyCalendarDay implements Serializable{

	public static SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
	public static DecimalFormat percentageFormat = new DecimalFormat("0.00");
	
	private boolean currentMonth;
	private Date date;
	private String dayDisplay;
	private Standby standby;
	private TimeEntry firstTimeEntry;
	private TimeEntry secondTimeEntry;
	private Integer index;

	public StandbyCalendarDay(Integer index,Date date, boolean currentMonth, Standby standby) {
		this.date = date;
		this.dayDisplay = dayFormat.format(date);
		this.currentMonth = currentMonth;
		this.standby = standby;
		this.index = index;
	}
	public Date getDate() {
		return date;
	}
	public String getDayDisplay() {
		return dayDisplay;
	}
	public static SimpleDateFormat getDayFormat() {
		return dayFormat;
	}
	public static void setDayFormat(SimpleDateFormat dayFormat) {
		StandbyCalendarDay.dayFormat = dayFormat;
	}
	public boolean isCurrentMonth() {
		return currentMonth;
	}
	public void setCurrentMonth(boolean currentMonth) {
		this.currentMonth = currentMonth;
	}
	public Standby getStandby() {
		return standby;
	}
	public void setStandby(Standby standby) {
		this.standby = standby;
	}
	public TimeEntry getFirstTimeEntry() {
		return firstTimeEntry;
	}
	public void setFirstTimeEntry(TimeEntry firstTimeEntry) {
		this.firstTimeEntry = firstTimeEntry;
	}
	public TimeEntry getSecondTimeEntry() {
		return secondTimeEntry;
	}
	public void setSecondTimeEntry(TimeEntry secondTimeEntry) {
		this.secondTimeEntry = secondTimeEntry;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public void setDayDisplay(String dayDisplay) {
		this.dayDisplay = dayDisplay;
	}
	public String getFirstDivHeight(){
		double height = ((standby.getStart()*60.0) / (24*60*60.0))*100;
		return percentageFormat.format(height).replace(",", ".");
	}
	public String getSecondDivHeight(){
		double height = 100 - ((standby.getStart()*60.0) / (24*60*60.0))*100;
		return percentageFormat.format(height).replace(",", ".");
	}
	public String getFirstDivColor(){
		String color = "white";
		if(firstTimeEntry != null && firstTimeEntry.getPerson() != null){
			color = firstTimeEntry.getPerson().getColor();
		}		
		return color;
	}
	public String getSecondDivColor(){
		String color = "white";
		if(secondTimeEntry != null && secondTimeEntry.getPerson() != null){
			color = secondTimeEntry.getPerson().getColor();
		}		
		return color;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	
}
