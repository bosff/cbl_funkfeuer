package de.bosc.fufeu.company.model;

import java.io.Serializable;

public class Company implements Serializable,Comparable<Company>{

	private String name;
	private String docId;
	private Integer cutOffTime = 0;
	private Integer sort = 0;
	private String color;
	private String textColor;
	private boolean acitive = false;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getCutOffTime() {
		return cutOffTime;
	}
	public void setCutOffTime(Integer cutOffTime) {
		this.cutOffTime = cutOffTime;
	}
	public boolean isAcitive() {
		return acitive;
	}
	public void setAcitive(boolean acitive) {
		this.acitive = acitive;
	}
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getTextColor() {
		return textColor;
	}
	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public int compareTo(Company o) {
		return this.sort < o.sort?1:0;
	}

}
