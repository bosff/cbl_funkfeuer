package de.bosc.fufeu.company.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class DatabaseConfig implements Serializable{

	private List<String> summaryOrgFunctions = new ArrayList<String>();
	private List<String> qualificationsGraph24 = new ArrayList<String>();
	private List<String> qualificationsSummary = new ArrayList<String>();
	private List<String> qualificationsWerkschutz = new ArrayList<String>();
	
	private boolean logging;
	private Integer logLevel = 3;
	private Integer graph24hMinimumPresence = 30*60;
	private String httpServername;

	public Integer getGraph24hMinimumPresence() {
		return graph24hMinimumPresence;
	}

	public String getHttpServername() {
		return httpServername;
	}

	public Integer getLogLevel() {
		return logLevel;
	}

	public List<String> getQualificationsGraph24() {
		return qualificationsGraph24;
	}

	public List<String> getQualificationsSummary() {
		return qualificationsSummary;
	}

	public List<String> getQualificationsWerkschutz() {
		return qualificationsWerkschutz;
	}

	public List<String> getSummaryOrgFunctions() {
		return summaryOrgFunctions;
	}

	public boolean isLogging() {
		return logging;
	}

	public void setGraph24hMinimumPresence(Integer graph24hMinimumPresence) {
		this.graph24hMinimumPresence = graph24hMinimumPresence;
	}

	public void setHttpServername(String httpServername) {
		this.httpServername = httpServername;
	}

	public void setLogging(boolean logging) {
		this.logging = logging;
	}

	public void setLogLevel(Integer logLevel) {
		this.logLevel = logLevel;
	}

	public void setQualificationsGraph24(List<String> qualificationsGraph24) {
		this.qualificationsGraph24 = qualificationsGraph24;
	}

	public void setQualificationsSummary(List<String> qualificationsSummary) {
		this.qualificationsSummary = qualificationsSummary;
	}

	public void setQualificationsWerkschutz(List<String> qualificationsWerkschutz) {
		this.qualificationsWerkschutz = qualificationsWerkschutz;
	}

	public void setSummaryOrgFunctions(List<String> summaryOrgFunctions) {
		this.summaryOrgFunctions = summaryOrgFunctions;
	}
}
