package de.bosc.fufeu.company.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;

import de.bosc.fufeu.company.business.DatabaseService;
import de.bosc.fufeu.company.business.PersonService;

public class StandbyCalendar implements Serializable{

	private TreeMap<Integer,StandbyCalendarDay> days = new TreeMap<Integer,StandbyCalendarDay>();
	private HashMap<String,Standby> listStandby = new HashMap<String, Standby>();
	private Standby currentStandby;
	private Person selectedPerson;
	private Date selectedDate = new Date();;
	private boolean editable = false;

	
	public StandbyCalendar() {
	}

	public TreeMap<Integer, StandbyCalendarDay> getDays() {
		return days;
	}

	public void setDays(TreeMap<Integer, StandbyCalendarDay> days) {
		this.days = days;
	}

	public HashMap<String, Standby> getListStandby() {
		return listStandby;
	}

	public void setListStandby(HashMap<String, Standby> listStandby) {
		this.listStandby = listStandby;
	}

	public Standby getCurrentStandby() {
		return currentStandby;
	}

	public void setCurrentStandby(Standby currentStandby) {
		this.currentStandby = currentStandby;
	}

	public Person getSelectedPerson() {
		return selectedPerson;
	}

	public void setSelectedPerson(Person selectedPerson) {
		this.selectedPerson = selectedPerson;
	}

	public Date getSelectedDate() {
		return selectedDate;
	}

	public void setSelectedDate(Date selectedDate) {
		this.selectedDate = selectedDate;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

}
