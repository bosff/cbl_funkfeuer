package de.bosc.fufeu.company.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import de.bosc.fufeu.company.business.DatabaseService;
import de.bosc.fufeu.company.business.TimeEntryService;
public class Graph24h implements Serializable{
	
	private static final long serialVersionUID = 1L;

	static public final int COLUMN_WIDTH = 40; 
	static public final int GRAPH_HEIGHT = 520; 
	static public final int HEADER_HEIGHT = 30; 
	static public ArrayList<Integer> getEmptyTotalValuesArray(){
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i=0;i<24;i++){
			arr.add(0);
		}
		return arr;
	}
	
	private ArrayList<GraphData> arrGraphData = new ArrayList<GraphData>();
	private ArrayList<Integer> arrTotalvalues = getEmptyTotalValuesArray();
	private ArrayList<String> colors = new ArrayList<String>();
	private ArrayList<String> textcolors = new ArrayList<String>();
	private int highestTotalvalue =0;
	private DatabaseService databaseService;
	
	
	public Graph24h(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}

	
	
	public String getSvgGraphic() {
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<svg width='1000' height='600'>");
			sb.append("<style>");
			for(String color:colors){
				sb.append(".data_" + color + "{");
				sb.append("fill:" + color + ";stroke-width:1");			
				sb.append("stroke:black}");			
			}
			sb.append("</style>");
			for(int i=0;i<25;i++){
				sb.append("<line class='horizontalline' x1='" + (i*COLUMN_WIDTH+10) + "' y1='0' x2='" + (i*COLUMN_WIDTH+10) + "' y2='" + (GRAPH_HEIGHT+HEADER_HEIGHT+10) + "' />");
				if(i<24){
					sb.append("<text class='totalvalues' x='" + (i*COLUMN_WIDTH+10+COLUMN_WIDTH/2) + "' y='20'>" + arrTotalvalues.get(i) + "</text>");
				}
				sb.append("<text class='xaxis' x='" + (i*COLUMN_WIDTH+10) + "' y='" + (GRAPH_HEIGHT+HEADER_HEIGHT + 20)+ "'>" + (i) + "</text>");
			}
			sb.append("<line class='horizontalline' x1='10' y1='" + HEADER_HEIGHT + "' x2='970' y2='" + HEADER_HEIGHT +"' />");
			sb.append("<line class='horizontalline' x1='10' y1='" + (GRAPH_HEIGHT+HEADER_HEIGHT) + "' x2='970' y2='" + (GRAPH_HEIGHT+HEADER_HEIGHT) + "' />");
			sb.append(addData());
			sb.append("</svg>");		
		}catch(Exception e){
			getDatabaseService().error(e);
		}
		return  sb.toString();			

	}
	
	private StringBuffer addData(){
		final StringBuffer sb = new StringBuffer();
		final int factor = highestTotalvalue == 0?0:GRAPH_HEIGHT/(highestTotalvalue+3);
		final int yZero = GRAPH_HEIGHT + HEADER_HEIGHT;			//Ursprung Koordinatensysten
		try{
			int height =0;
			ArrayList<Integer> dataBase = new ArrayList<Integer>();
			ArrayList<Integer> dataLast = new ArrayList<Integer>();
			for(int i=0;i<24;i++){
				dataBase.add(0);
				dataLast.add(yZero);
			}
			for(int i=0;i<colors.size();i++){
				String styleClass = "class='data_" + colors.get(i)  + "'";
				ArrayList<Integer> data = arrGraphData.get(i).getData();
				for(int j=0;j<24;j++){
					dataBase.set(j, dataBase.get(j) + data.get(j));				
					int y = (GRAPH_HEIGHT + 3) - (factor*dataBase.get(j)) + HEADER_HEIGHT;
					int x = (j*COLUMN_WIDTH+10+COLUMN_WIDTH/2);
	
					height = dataLast.get(j)-y;
					dataLast.set(j,y);
					
					sb.append("<rect style='fill:" +  colors.get(i)  + "'" + styleClass + " x='" + (x-20) + "' y='" + (y-5) + "' width='40' height='" + height + "'/>");		
					if(data.get(j) != 0){
						sb.append("<text style='fill:" +  textcolors.get(i)  + ";font-weight:bold' class='data' x='" + x + "' y='" + (y+2) + "'>" + data.get(j) + "</text>");
					}
				}
			}
		}catch(Exception e){
			getDatabaseService().error(e);
		}
		return sb;
	}
	
	public void addGraphData(GraphData graphData){
		arrGraphData.add(graphData);
		colors.add(graphData.getColor());
		textcolors.add(graphData.getTextcolor());
		for(int i=0;i<24;i++){
			int newValue = arrTotalvalues.get(i) + graphData.getData().get(i);
			arrTotalvalues.set(i,newValue );
			if(highestTotalvalue < newValue){
				highestTotalvalue = newValue;
			}
		}
		getDatabaseService().debug("Graph24h.addGraphData highestTotalvalue = " + highestTotalvalue + " ---- DATA: "  + graphData.getData());
	}
	
	public ArrayList<GraphData> getArrGraphData() {
		return arrGraphData;
	}

	public void setArrGraphData(ArrayList<GraphData> arrGraphData) {
		this.arrGraphData = arrGraphData;
	}

	public ArrayList<Integer> getArrTotalvalues() {
		return arrTotalvalues;
	}

	public void setArrTotalvalues(ArrayList<Integer> arrTotalvalues) {
		this.arrTotalvalues = arrTotalvalues;
	}

	public DatabaseService getDatabaseService() {	
		return databaseService;
	}

	public void setDatabaseService(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}


	public class GraphData implements Serializable{
		private static final long serialVersionUID = 1L;
		private int totalvalue =0;
		private String color = "#4499AA";
		private String textcolor = "black";
		private ArrayList<Integer> data = new ArrayList<Integer>();
		
		public GraphData() {
			for(int j=0;j<24;j++){
				data.add(0);
			}
		}
		
		public GraphData(String color,ArrayList<Integer> data) {
			this.data = data;
			this.color = color;
			for(Integer value:data){
				this.totalvalue +=value;				
			}
		}
		
		public void addTimeEntry(TimeEntry timeEntry, StringBuilder sb){
			try{
				Calendar calStart = Calendar.getInstance();
				Calendar calEnd = Calendar.getInstance();
				calStart.setTime(TimeEntryService.parseTimeEntryDate(timeEntry.getDate(),timeEntry.getStarttimeText()));
				if(timeEntry.getEndtime() == 86400){
					calEnd.set(Calendar.HOUR_OF_DAY,23);
					calEnd.set(Calendar.MINUTE,59);
				}else if(timeEntry.getEndtime() != -1){
					// Zeiteintrag ist vollst�ndig -> alle Stunden zwischen Start und Ende hochgez�hlt
					calEnd.setTime(TimeEntryService.parseTimeEntryDate(timeEntry.getDate(),timeEntry.getEndtimeText()));
				}else if(TimeEntryService.getStringValueNow().equals(timeEntry.getDate())){
					// heutiger offener Zeitenitrag -> Ende Zeit = jetzt
					calEnd.setTime(new Date());				
				}else{
					// ansich fehlerhafter Zeitentrag -> 24:00 als Ende annehmen
					calEnd.set(Calendar.HOUR_OF_DAY,23);
					calEnd.set(Calendar.MINUTE,59);
				}
				int i = 0;
				int startHour = calStart.get(Calendar.HOUR_OF_DAY);
				sb.append("\n ----- addTimeEntry start:" + startHour + " - End:" + calEnd.get(Calendar.HOUR_OF_DAY ));
				while(calStart.getTime().before(calEnd.getTime()) && i < 24){
//				while((calStart.get(Calendar.HOUR_OF_DAY) <= calEnd.get(Calendar.HOUR_OF_DAY )) && i < 24){
					int secondsPresent = 3600;
					int secondsStart = calStart.get(Calendar.MINUTE)*60 + calStart.get(Calendar.SECOND);
					int secondsEnd = calEnd.get(Calendar.MINUTE)*60 + calEnd.get(Calendar.SECOND);
					if(calStart.get(Calendar.HOUR_OF_DAY) == calEnd.get(Calendar.HOUR_OF_DAY) ){
						if(calEnd.get(Calendar.HOUR_OF_DAY) > startHour){
							secondsPresent = secondsEnd;
						}else{
							secondsPresent = secondsEnd - secondsStart;
						}
					} else if(calStart.get(Calendar.HOUR_OF_DAY) == startHour){
						secondsPresent = 3600 - secondsStart;
					} 
					sb.append("\n ---- " + calStart.getTime() + ": secondsPresent = " + secondsPresent + " --- minvalue: " + databaseService.getDatabaseConfig().getGraph24hMinimumPresence() + " --- end:" + calEnd.getTime());
					if(databaseService.getDatabaseConfig().getGraph24hMinimumPresence()<secondsPresent){
						int pos = calStart.get(Calendar.HOUR_OF_DAY);
						Integer currentValue = data.get(pos);
						currentValue++;
						data.set(pos,currentValue);						
					}
					calStart.add(Calendar.HOUR,1);
					i++;	
				}
				
			}catch(Exception e){
				getDatabaseService().error(e);
			}
		}
//
//		public void setData(int pos, Integer value){
//			Integer currentValue = data.get(pos);
//			this.totalvalue -=currentValue;
//			this.totalvalue +=value;
//			currentValue = value;
//		}
		
		public ArrayList<Integer> getData() {
			return data;
		}
		public String getSvgGraph(){
			return "";
		}
		public String getColor() {
			return color;
		}
		public int getTotalvalue() {
			return totalvalue;
		}
		public void setColor(String color) {
			this.color = color;
		}
		public void setData(ArrayList<Integer> data) {
			this.data = data;
		}
		public String getTextcolor() {
			return textcolor;
		}

		public void setTextcolor(String textcolor) {
			this.textcolor = textcolor;
		}
	}
	
}