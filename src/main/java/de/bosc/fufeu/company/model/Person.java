package de.bosc.fufeu.company.model;

import java.io.Serializable;
import java.util.List;

public class Person implements Serializable{

	private String personalNumber;
	private String lastname;
	private String firstname;
	private String department;
	private String shortname;
	
	private String company;
	private List<String> orgFunctions;
	private List<String> qualifications;
	private Integer prio;
	private Integer cutOffTime = 0;
	private String color = "�ff0000";
	
	private String phoneNumberBusiness;
	private String phoneNumber;
	private String mobileNumber;
	private String unid;
	
	private boolean active = false;
	
	
	private boolean valid = false;

	public String getPersonalNumber() {
		return personalNumber;
	}
	public void setPersonalNumber(String personalNumber) {
		this.personalNumber = personalNumber;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public List<String> getOrgFunctions() {
		return orgFunctions;
	}
	public void setOrgFunctions(List<String> orgFunctions) {
		this.orgFunctions = orgFunctions;
	}
	public Integer getPrio() {
		return prio;
	}
	public void setPrio(Integer prio) {
		this.prio = prio;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public String getFullname(){
		return getLastname() + ", " + getFirstname();
	}
	public Integer getCutOffTime() {
		return cutOffTime;
	}
	public void setCutOffTime(Integer cutOffTime) {
		this.cutOffTime = cutOffTime;
	}
	public List<String> getQualifications() {
		return qualifications;
	}
	public void setQualifications(List<String> qualifications) {
		this.qualifications = qualifications;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getShortname() {
		return shortname;
	}
	public void setShortname(String shortname) {
		this.shortname = shortname;
	}
	@Override
	public String toString() {
		return lastname + ", " + firstname;
	}
	public String getPhoneNumberBusiness() {
		return phoneNumberBusiness;
	}
	public void setPhoneNumberBusiness(String phoneNumberBusiness) {
		this.phoneNumberBusiness = phoneNumberBusiness;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getUnid() {
		return unid;
	}
	public void setUnid(String unid) {
		this.unid = unid;
	}
	
}
