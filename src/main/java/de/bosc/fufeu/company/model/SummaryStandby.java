package de.bosc.fufeu.company.model;

public class SummaryStandby {
	
	private Standby standby;
	private TimeEntry timeEntry;
	
	public Standby getStandby() {
		return standby;
	}
	public void setStandby(Standby standby) {
		this.standby = standby;
	}
	public TimeEntry getTimeEntry() {
		return timeEntry;
	}
	public void setTimeEntry(TimeEntry timeEntry) {
		this.timeEntry = timeEntry;
	}
}
