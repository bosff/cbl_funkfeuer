package de.bosc.fufeu.company.model;

import java.io.Serializable;

public class SummaryOrgFunction implements Serializable{

	
	private Listentry orgFunction;
	private Person person;
	
	public Listentry getOrgFunction() {
		return orgFunction;
	}
	public void setOrgFunction(Listentry orgFunction) {
		this.orgFunction = orgFunction;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	
	
	
}
