package de.bosc.fufeu.company.model;

import java.io.Serializable;
import java.util.List;

public class QualificationGroup implements Serializable {

	private String name;
	private String alias;
	private String docId;
	private String display;
	private Integer sort;
	private String style;
	private List<String> list;
	
	public String getAlias() {
		return alias;
	}
	public String getDisplay() {
		return display;
	}
	public String getDocId() {
		return docId;
	}
	public List<String> getList() {
		return list;
	}
	public String getName() {
		return name;
	}
	public Integer getSort() {
		return sort;
	}
	public String getStyle() {
		return style;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
	public void setList(List<String> list) {
		this.list = list;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	@Override
	public String toString() {
		return "QualificationGroup [name=" + name + "]";
	}

}
