package de.bosc.fufeu.company.model;


import java.io.Serializable;
import java.util.Vector;
public class Listentry implements Serializable{

	private String name;
	private String alias;
	private String docId;
	private String display;
	private Integer sort;
	private String style;
	private Vector<String> includedListentries;
	private boolean xPageEditable = false;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public boolean isXPageEditable() {
		return xPageEditable;
	}
	public void setXPageEditable(boolean pageEditable) {
		xPageEditable = pageEditable;
	}
	public Vector<String> getIncludedListentries() {
		return includedListentries;
	}
	public void setIncludedListentries(Vector<String> includedListentries) {
		this.includedListentries = includedListentries;
	}
	@Override
	public String toString() {
		return getDisplay();
	}
}
