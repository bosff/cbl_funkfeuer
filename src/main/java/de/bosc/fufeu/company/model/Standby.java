package de.bosc.fufeu.company.model;

import java.io.Serializable;

public class Standby implements Serializable{

	private String name;
	private int start;
	private boolean active;
	private String docid;
	private String displayname;
	private String alias;
	private String orgFunction;
	
	@Override
	public String toString() {
		return "Standby [name=" + name + "]";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getDocid() {
		return docid;
	}
	public void setDocid(String docid) {
		this.docid = docid;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getOrgFunction() {
		return orgFunction;
	}
	public void setOrgFunction(String orgFunction) {
		this.orgFunction = orgFunction;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
}
