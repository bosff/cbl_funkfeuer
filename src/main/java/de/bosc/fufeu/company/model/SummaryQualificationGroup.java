package de.bosc.fufeu.company.model;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

public class SummaryQualificationGroup implements Serializable{

	private QualificationGroup qualificationGroup;
	private Map<String,Person> listPersons  = new TreeMap<String, Person>();
	
	public QualificationGroup getQualificationGroup() {
		return qualificationGroup;
	}
	public void setQualificationGroup(QualificationGroup qualificationGroup) {
		this.qualificationGroup = qualificationGroup;
	}
	public Map<String, Person> getListPersons() {
		return listPersons;
	}
	public void setListPersons(Map<String, Person> listPersons) {
		this.listPersons = listPersons;
	}
	
	
	
}
